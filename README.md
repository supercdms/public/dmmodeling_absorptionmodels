# DM Modeling AbsorptionModels Package

Repository and python package for computing dark absorption model signal models, including dark-photon absorption (DPA) and axion-like particle (ALP) absorption.

This package is made available by the SuperCDMS Collaboration to provide in the public domain the software used for SuperCDMS studies. Though validated by the Collaboration, there is no warranty or guarantee that it is correct. Users should perform their own validation of the functions contained within this package.

Contacts - Taylor Aralis (taralis@caltech.edu, tbaralis@gmail.com), Matthew Wilson (matthew.wilson@kit.edu)

---

## Table of Contents
  1. [Installation](#install)
      1. [Install Dependencies](#install_dep)
  2. [Dark Photon Absorption Tool](#dpa)
  3. [ALP Absorption Tool](#alp)
  4. [Project Status](#status)
  5. [Issue Tracking](#issue_tracking)

---

## Installation <a name="install"></a>
#### Install Dependencies: <a name="install_dep"></a>
-  numpy
-  [dmmodeling_physicsvalues >= 2.2.0](https://gitlab.com/supercdms/public/dmmodeling_physicsvalues)
-  [dmmodeling_opticalconstants >= 1.2.5](https://gitlab.com/supercdms/public/dmmodeling_opticalconstants)

This git repository is also an installable python package. There are various ways to install a python package, but a common way is to first git clone the repository into your local directory. Once that is done, the package can be installed as follows:
```
pip install [path-to-dmmodeling_absorptionmodels-package]/dmmodeling_absorptionmodels
```
or
```
pip install --upgrade [path-to-dmmodeling_absorptionmodels-package]/dmmodeling_absorptionmodels
```
Depending on the user's setup, the installation may or may not be done within a python venv. In some cases, it may be necessary to add the `--user` argument to the pip install command. If the package is installed from a Juptyer notebook, restart the kernal after the package is successfully installed before importing.

This package has an installation dependencies on another custom python package(s) ([dmmodeling_physicsvalues](https://gitlab.com/supercdms/public/dmmodeling_physicsvalues), [dmmodeling_opticalconstants](https://gitlab.com/supercdms/public/dmmodeling_opticalconstants)). The installation will attempt to find if the package(s) exists in the correct version(s) and if not, attempt to retrieve the package(s) from Gitlab and install them. This installation process requires the git executable to be available on the system path. ***In some cases, the installation fails to access the dependent package(s) from GitLab and fails to install them. If this happens, the user should manually install the dependent package(s) with the correct version.***


***Package installation works with pip version 21.3.1. Try updating pip if the installation fails.***

---

## Dark Photon Absorption Tool <a name="dpa"></a>
The model for dark photon absorption follows the description given by [Hochberg et. al](https://journals.aps.org/prd/abstract/10.1103/PhysRevD.95.023013). 

The model assumes all relic dark matter is composed of non-relativistic vector bosons (a.k.a dark photons) with a mass $m_{A'}$. This massive vector boson of a Dark Gauge group U(1)D may kinetically mix with the electromagnetic/hypercharge U(1)γ Standard Model group with a mixing parameter $\varepsilon$. The rate calculation for dark photon absorption is given by:

$$R= \frac{1}{\rho_{tm}} \frac{\rho_{DM}}{m_{A^{\prime}}} \varepsilon_{eff}^{2} \sigma_{1}(\omega = m_{A^{\prime}}),$$

where $\rho_{tm}$ is the density of the target material, $\rho_{DM}$ is the local dark matter density, $\sigma_{1}(m_{A'})$ is the energy-dependent real part of the complex optical conductivity, and $\varepsilon_{eff}$ is the effective dark photon mixing parameter that accounts for in-medium effects. The effective mixing parameter is related to the mixing parameter $\varepsilon$ by:

$$\varepsilon_{eff}^{2} = \frac{\varepsilon^{2} m_{A^{\prime}}^{2}}{(m_{A^{\prime}}^{2} - 2m_{A^{\prime}} \sigma_{2} + \sigma_{2}^{2} + \sigma_{1}^{2})},$$

where $\sigma_{2}$ is the imaginary part of the complex conductivity. This tool uses the [physicsvalues](https://gitlab.com/supercdms/physicsvalues), [opticalconstants](https://gitlab.com/supercdms/MaterialProperties/opticalconstants) to load relevant constants and parameters.

Generating the expected interaction rate for various dark photon masses can be done using the `compute_DPA_rate` function with the following arguments:

**Parameters**
  - abs_masses : 1D numpy array
      - array of dark photon masses in keV/c^2.
  - temperature : int or float
      - temperature of the target material. Required for calculating the indirect absorption model in Si in the opticalconstants package (relevant for absorption energies below ~4 eV).
    
**Optional Parameters**
  - target : string, default = 'Si'
      - target material to compute the interaction rate. Options are: 'Si'.
  - eps0 : float, default = 5e-13
      - kinetic mixing parameter for dark photon absorption (unitless).
  - physicsvalues_obj : Dictionary, default = None
      - an object of the Dictionary class with the constants. Can be obtained from physicsvalues.load_constants() If None, the constants from physicsvalues.default_constants are used.
  - sig1 : 1D numpy array, default = None
      - real part of the complex conductivity used to calculate the dark photon absorption interaction rate. If None, this information is loaded from the opticalconstants package.If passed, it must have the same length as the abs_masses argument and should be in units of eV.
  - sig1_unc : string, default = 'nominal'
      - for the fitted model describing the temperature-dependent region of the indirect absorption model in Si at energies below ~4 eV, whether the nominal, upper, or lower uncertainty curve is returned. Options are 'nominal', 'lower', or 'upper'.
    
**Returns**
  - DPA_rate : 1D numpy array
      - computed dark photon absorption interaction rate for each dark photon mass in abs_masses in units of per gram-days.


An example of using this tool is shown below.

```python
import absorptionmodels as absmodels
import numpy as np

# define grid of dark photon masses in units keV/c^2
abs_masses = np.logspace(np.log10(1.2e-3),np.log10(1e-1),200)

temperature = 0.05 # target material temperature

# compute dark photon absorption rate using default settings
dp_absorption_rate = absmodels.compute_DPA_rate(abs_masses,temperature)

```

Executing the code above will result in the curve shown in the plot below.

![DPA_rate_example](Images/DPA_rate_example.png)


---

## ALP Absorption Tool <a name="alp"></a>
The model for ALP absorption follows (by default) the descriptions provided by [Hochberg et. al](https://journals.aps.org/prd/abstract/10.1103/PhysRevD.95.023013). 

This model assumes all relic dark matter is composed of non-relativistic axion-like particles (ALP) with mass $m_{a}$. The dark absorption of an ALP via the axio-electric effect would eject a bound electron from an atom. The effective ALP-electron interaction is quantified by the axio-electric coupling $g_{ae}$, and the expected cross section is proportional to the photoelectric cross-section. For non-relativistic ALPs, the event rate expressed as a function of $g_{ae}$ is given by

$$R=\frac{\rho_{DM}}{\rho_{tm}} \frac{3 g_{ae}^{2} \cdot m_{a}}{16 \pi \alpha \hbar m_{e}^{2}} \sigma_{1}(m_{a}),$$

where $\rho_{DM}$ is the local dark matter density, $\rho_{tm}$ is the target density $m_{e}$ is the electron mass, $\alpha$ is the fine structure constant, and $\sigma_{1}$ is the real part of the complex conductivity, dependent on the ALP mass. This tool uses the [physicsvalues](https://gitlab.com/supercdms/physicsvalues), [opticalconstants](https://gitlab.com/supercdms/MaterialProperties/opticalconstants) to load relevant constants and parameters.

By default, the rate equation is derived from the same source as the rate equation for dark photon absorption in [Hochberg et. al](https://journals.aps.org/prd/abstract/10.1103/PhysRevD.95.023013) and includes in-medium effects. However, this tool provides the optional input argument `include_inmedium_effects` which can be set to `False` to remove in-medium effects, following the approach taken in [Pospelov et. al.](https://journals.aps.org/prd/abstract/10.1103/PhysRevD.78.115012), [Fu et. al.](https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.119.181806), and [Arisaka et. al.](https://www.sciencedirect.com/science/article/abs/pii/S0927650512002344). The only difference between the two approaches is a factor of $n$, the index of refraction of the target material: 

$$R_{\mathrm{Without \, in-medium \, effects}}=R_{\mathrm{With \, in-medium \, effects}}/n = \rho_{DM} \frac{3 g_{ae}^{2} c \cdot m_{a}}{16 \pi \alpha m_{e}^{2}} \sigma_{p.e.}(m_{a}),$$

where $\sigma_{p.e.}$ is the photoelectric absorption cross section in units of cm$^{2}$/g. This difference is only relevant for ALP masses below $\sim 50$ eV/$c^{2}$.


Generating the expected interaction rate for various dark photon masses can be done using the `compute_ALP_rate` function with the following arguments:

**Parameters**
  - abs_masses : 1D numpy array
      - array of ALP masses in keV/c^2.
  - temperature : int or float
      - temperature of the target material. Required for calculating the indirect absorption model in Si in the opticalconstants package (relevant for absorption energies below ~4 eV).

**Optional Parameters**
  - target : string, default = 'Si'
      - target material to compute the interaction rate. Options are: 'Si'.
  - gae0 : float, default = 5e-11
      - axio-electric coupling constant for ALP absorption (unitless).
  - include_inmedium_effects : bool, default = True
      - whether or not to include in-medium effects into the rate equation. If true, the rate follows equation 10 from [Hochberg et. al.](https://arxiv.org/pdf/1608.01994.pdf). If False, the model follows that of [Fu et. al](https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.119.181806) and [Arisaka et. al.](https://www.sciencedirect.com/science/article/abs/pii/S0927650512002344). The approaches differ by only a factor of n (index of refraction).
  - physicsvalues_obj : Dictionary, default = None
      - an object of the Dictionary class with the constants. Can be obtained from physicsvalues.load_constants() If None, the constants from physicsvalues.default_constants are used.
  - sig1 : 1D numpy array, default = None
      - real part of the complex conductivity used to calculate the ALP absorption interaction rate. If None, this information is loaded from the opticalconstants package. If passed, it must have the same length as the abs_masses argument and should be in units of eV.
  - sig1_unc : string, default = 'nominal'
    - for the fitted model describing the temperature-dependent region of the indirect absorption model in Si at energies below ~4 eV, whether the nominal, upper, or lower uncertainty curve is returned. Options are 'nominal', 'lower', or 'upper'.

**Returns**
  - ALP_rate : 1D numpy array
      - computed ALP absorption interaction rate for each ALP mass in abs_masses in units of per gram-days.

An example of using this tool is shown below.

```python
import absorptionmodels as absmodels
import numpy as np

# define grid of ALP masses in units keV/c^2
abs_masses = np.logspace(np.log10(1.2e-3),np.log10(1e-1),200)

temperature = 0.05 # target material temperature

# compute dark photon absorption rate using default settings
ALP_absorption_rate = absmodels.compute_ALP_rate(abs_masses,temperature)

```

Executing the code above will result in the curve shown in the plot below.

![ALP_rate_example](Images/ALP_rate_example.png)


---

## Project Status <a name="status"></a>
Potential or known updates include:
-  including abiliity to compute dark absorption rates for other target materials, specifically Germanium.

---

## Issue Tracking <a name="issue_tracking"></a>
For reporting issue with this package or requesting features, please submit a new issue for this project on GitLab. Unfortunately, merge requests are not currently supported for public users of this package. If you are a public user of this package and would like to submit a merge request, please contact the project manager.
