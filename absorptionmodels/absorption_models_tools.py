import numpy as np
from scipy.interpolate import interp1d

import opticalconstants as opt_tools
import physicsvalues as pvl

__all__ = [
    "compute_DPA_rate",
    "compute_ALP_rate",
]

def unbox_constants(constants,target):
    '''
    Simple function to grab the necessary constants from the 
    physicsvalues Dictionary object for these signal models.
    '''
    
    c = constants.physics_params.speed_of_light.value # speed of light (cm/s)
    hbar = constants.physics_params.reduced_planck.value # reduced Planck constant (eVs)
    alpha = constants.physics_params.fine_structure.value #fine structure constant
    me = constants.physics_params.e_mass.value # electron mass (keV)
    rhoDM = constants.halo_params.DM_density.value # DM density (GeV/cm^3)
    
    if target.lower() == 'si':
        rho = constants.Si_params.Si_density.value # target density (kg/m^3)
        
    return c, hbar, alpha, me, rhoDM, rho

def compute_DPA_rate(abs_masses,
                     temperature,
                     target="Si",
                     eps0=5e-13,
                     physicsvalues_obj=None,
                     sig1=None,
                     sig1_unc = 'nominal'):
    """
    Calculates the expected interaction rate of dark photon absorption 
    for given dark photon masses, kinetic mixing parameter and target material.
    The rate is returned in units of per gram-days.
    
    TODO: add more target material options (e.g. Ge).

    Parameters
    ----------
    abs_masses : 1D numpy array
        array of dark photon masses in keV/c^2.
    temperature : int or float
        temperature of the target material. Required for calculating the 
        indirect absorption model in Si in the opticalconstants package 
        (relevant for absorption energies below ~4 eV).
    
    Optional Parameters
    -------------------
    target : string, default = 'Si'
        target material to compute the interaction rate. Options are: 'Si'.
    eps0 : float, default = 5e-13
        kinetic mixing parameter for dark photon absorption (unitless).
    physicsvalues_obj : Dictionary, default = None
        an object of the Dictionary class with the constants. Can be obtained 
        from physicsvalues.load_constants() If None, the constants from 
        physicsvalues.default_constants are used.
    sig1 : 1D numpy array, default = None
        real part of the complex conductivity used to calculate the dark 
        photon absorption interaction rate. If None, this information is 
        loaded from the opticalconstants package.If passed, it must have 
        the same length as the abs_masses argument and should be in units of eV.
    sig1_unc : string, default = 'nominal'
        for the fitted model describing the temperature-dependent region of 
        the indirect absorption model in Si at energies below ~4 eV, whether 
        the nominal, upper, or lower uncertainty curve is returned. 
        Options are 'nominal', 'lower', or 'upper'.
    
    Returns
    -------
    DPA_rate : 1D numpy array
        computed dark photon absorption interaction rate for each dark photon 
        mass in abs_masses in units of per gram-days.
    """
    
    # check abs_masses
    if isinstance(abs_masses,list):
        abs_masses = np.asarray(abs_masses)
    elif isinstance(abs_masses,int) or isinstance(abs_masses,float):
        abs_masses = np.asarray([abs_masses])
    if isinstance(abs_masses,np.ndarray):
        if abs_masses.ndim != 1:
            raise ValueError('ERROR: abs_masses argument must be an array '+\
                             'with 1 dimension.')
        if any(abs_masses<0):
            raise ValueError('ERROR: abs_masses argument must contain '+\
                             'only postive numbers.')
    else:
        raise TypeError('ERROR: abs_masses argument is not or cannot be '+\
                        'converted to a numpy 1D array.')
    # check to make sure other arguments are okay
    if not isinstance(temperature,int) and not isinstance(temperature,float):
        raise TypeError('ERROR: temperature argument must be either of int or float type.')
    elif temperature < 0:
         raise ValueError('ERROR: temperatureerature value of %s falls below zero. '%temperature+\
                          'Make sure the temperature is given in units of Kelvin!')
    if sig1_unc.lower() != 'nominal' and sig1_unc.lower() != 'upper' and sig1_unc.lower() != 'lower':
        if isinstance(sig1_unc,str):
            raise ValueError("ERROR: the sig1_unc argument '%s' is not accepted. "%sig1_unc+\
                             "Accepted options include 'nominal', 'upper', or 'lower'!")
        else:
            raise TypeError('ERROR: sig1_unc argument must be a string.')
    if not isinstance(target,str):
        raise TypeError('ERROR: target argument must be a string type.')
    elif target.lower() != 'si':
        raise NotImplementedError('ERROR: target argument must be one '+\
                                  'of the following options: "Si".')
    if not isinstance(eps0,float):
        raise TypeError('ERROR: eps0 argument must be a float type.')
    elif eps0<0:
        raise ValueError('ERROR: eps0 argument must be positive.')
    
    # decide which set of constants to use
    if physicsvalues_obj is None:
        # use pre-loaded constants
        physicsvalues_obj = pvl.default_constants
    elif not isinstance(physicsvalues_obj,pvl.Dictionary) and not isinstance(physicsvalues_obj,pvl.ImmutableDictionary):
        raise TypeError('ERROR: physicsvalues_obj was passed but it '+\
                        'is not of type physicsvalues Dictionary class.')
    
    # check sig1 and load if necessary
    if sig1 is None:
        if target.lower() == 'si':
            sig1 = opt_tools.grab_pexsec_Si(abs_masses,temperature,values=sig1_unc,param='sig1',physicsvalues_obj=physicsvalues_obj)
    else:
        if isinstance(sig1,list):
            sig1 = np.asarray(sig1)
        elif isinstance(sig1,int) or isinstance(sig1,float):
            sig1 = np.asarray([sig1])
        if isinstance(sig1,np.ndarray):
            if sig1.ndim != 1:
                raise ValueError('ERROR: sig1 argument must be an array '+\
                                 'with 1 dimension.')
        else:
            raise TypeError('ERROR: sig1 argument is not or cannot be '+\
                            'converted to a numpy 1D array.')
        if len(sig1) != len(abs_masses):
            raise ValueError('ERROR: the sig1 argument was passed but '+\
                             'does not match the length of abs_masses.')
    
    # load sig2 data
    sig2 = opt_tools.grab_conductivity_imaginary(abs_masses,target,physicsvalues_obj=physicsvalues_obj)
    
    # grab the necessary constants
    c, hbar, alpha, me, rhoDM, rho = unbox_constants(physicsvalues_obj,target)
    
    # compute in-medium effect correction factor
    corr_factor = np.sqrt(abs_masses*abs_masses*1e6 - 2*abs_masses*1e3*sig2 + sig2*sig2 + sig1*sig1)/(abs_masses*1e3)
    
    # DPA rate proportional to eps^2
    epsSq0 = np.power(eps0,2.0)
    
    # compute the DPA rate
    # DPA prescale units: [GeV/cm^3]*[keV/GeV]*[eV]*[1/eVs]*[m^3/kg]*[cm^3/m^3]*[kg/g]*[1/keV]*[s/day] = per g-day.
    DPA_rate = rhoDM*1e6*epsSq0*sig1*(60*60*24)/(hbar*rho*1e-3*abs_masses*(corr_factor**(2.0)))
    
    return DPA_rate

def compute_ALP_rate(abs_masses,
                     temperature,
                     target="Si",
                     gae0=5e-11,
                     include_inmedium_effects=True,
                     physicsvalues_obj=None,
                     sig1=None,
                     sig1_unc = 'nominal'):
    
    """
    Calculates the expected interaction rate of axion-like particle (ALP) 
    absorption for given ALP masses, axio-electic coupling constant, and 
    target material. The rate is returned in units of per gram-days.
    
    TODO: add more target material options (e.g. Ge).

    Parameters
    ----------
    abs_masses : 1D numpy array
        array of ALP masses in keV/c^2.
    temperature : int or float
        temperature of the target material. Required for calculating the indirect
        absorption model in Si in the opticalconstants package (relevant for 
        absorption energies below ~4 eV).
    
    Optional Parameters
    -------------------
    target : string, default = 'Si'
        target material to compute the interaction rate. Options are: 'Si'.
    gae0 : float, default = 5e-11
        axio-electric coupling constant for ALP absorption (unitless).
    include_inmedium_effects : bool, default = True
        whether or not to include in-medium effects into the
        rate equation. If true, the rate follows equation 10 
        from Hochberg et. al. (https://arxiv.org/pdf/1608.01994.pdf).
        If False, the model follows that of Fu et. al
        (https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.119.181806)
        and Arisaka et. al.
        (https://www.sciencedirect.com/science/article/abs/pii/S0927650512002344). 
        
        The approaches differ by only a factor of n (index of refraction).
        
    physicsvalues_obj : Dictionary, default = None
        an object of the Dictionary class with the constants. Can be 
        obtained from physicsvalues.load_constants() If None, the 
        constants from physicsvalues.default_constants are used.
    sig1 : 1D numpy array, default = None
        real part of the complex conductivity used to calculate the 
        ALP absorption interaction rate. If None, this information is 
        loaded from the opticalconstants package. If passed, it must 
        have the same length as the abs_masses argument and should be in units of eV.
    sig1_unc : string, default = 'nominal'
        for the fitted model describing the temperature-dependent region 
        of the indirect absorption model in Si at energies below ~4 eV, 
        whether the nominal, upper, or lower uncertainty curve is returned. 
        Options are 'nominal', 'lower', or 'upper'.
    
    Returns
    -------
    ALP_rate : 1D numpy array
        computed ALP absorption interaction rate for each ALP mass in 
        abs_masses in units of per gram-days.
    """
    
    
    # check abs_masses
    if isinstance(abs_masses,list):
        abs_masses = np.asarray(abs_masses)
    elif isinstance(abs_masses,int) or isinstance(abs_masses,float):
        abs_masses = np.asarray([abs_masses])
    if isinstance(abs_masses,np.ndarray):
        if abs_masses.ndim != 1:
            raise ValueError('ERROR: abs_masses argument must '+\
                             'be an array with 1 dimension.')
        if any(abs_masses<0):
            raise ValueError('ERROR: abs_masses argument must '+\
                             'contain only postive numbers.')
    else:
        raise TypeError('ERROR: abs_masses argument is not or cannot be '+\
                        'converted to a numpy 1D array.')
    # check to make sure other arguments are okay
    if not isinstance(temperature,int) and not isinstance(temperature,float):
        raise TypeError('ERROR: temperature argument must be either of int or float type.')
    elif temperature < 0:
         raise ValueError('ERROR: temperature value of %s falls below zero. '%temperature+\
                          'Make sure the temperature is given in units of Kelvin!')
    if sig1_unc.lower() != 'nominal' and sig1_unc.lower() != 'upper' and sig1_unc.lower() != 'lower':
        if isinstance(sig1_unc,str):
            raise TypeError("ERROR: the sig1_unc argument '%s' is not accepted. "%sig1_unc+\
                            "Accepted options include 'nominal', 'upper', or 'lower'!")
        else:
            raise ValueError('ERROR: sig1_unc argument must be a string.')
    if not isinstance(target,str):
        raise TypeError('ERROR: target argument must be a string type.')
    elif target.lower() != 'si':
        raise NotImplementedError('ERROR: target argument must be one '+\
                                  'of the following options: "Si".')
    if not isinstance(gae0,float):
        raise TypeError('ERROR: gae0 argument must be a float type.')
    elif gae0<0:
        raise ValueError('ERROR: gae0 argument must be positive.')
    if not isinstance(include_inmedium_effects,bool):
        raise TypeError('ERROR: include_inmedium_effects argument must be a boolean')
    
    # decide which set of constants to use
    if physicsvalues_obj is None:
        # use pre-loaded constants
        physicsvalues_obj = pvl.default_constants
    elif not isinstance(physicsvalues_obj,pvl.Dictionary) and not isinstance(physicsvalues_obj,pvl.ImmutableDictionary):
        raise TypeError('ERROR: physicsvalues_obj was passed but it is '+\
                        'not of type physicsvalues Dictionary class.')
    
    # check sig1 and load if necessary
    if sig1 is None:
        if target.lower() == 'si':
            # if include_inmedium_effects, grab the sig1 parameter in units of eV
            sig1 = opt_tools.grab_pexsec_Si(abs_masses,temperature,values=sig1_unc,param='sig1',physicsvalues_obj=physicsvalues_obj)
    else:
        if isinstance(sig1,list):
            sig1 = np.asarray(sig1)
        elif isinstance(sig1,int) or isinstance(sig1,float):
            sig1 = np.asarray([sig1])
        if isinstance(sig1,np.ndarray):
            if sig1.ndim != 1:
                raise ValueError('ERROR: sig1 argument must be an '+\
                                 'array with 1 dimension.')
        else:
            raise TypeError('ERROR: sig1 argument is not or cannot be '+\
                            'converted to a numpy 1D array.')
        if len(sig1) != len(abs_masses):
            raise ValueError('ERROR: the sig1 argument was passed but does '+\
                             'not match the length of abs_masses.')
    
    if not include_inmedium_effects:
        # if not include_inmedium_effects, need to remove factor of n
        n_data = opt_tools.load_data(target,'index_of_refraction_data',physicsvalues_obj)
        # this will also extrapolate the data. For energies above 1 keV, n --> 1.
        n_interp = interp1d(n_data[:,0],n_data[:,1],bounds_error=False,fill_value=(n_data[-1,1],1))
        n_use = n_interp(abs_masses)
    
    # grab the necessary constants
    c, hbar, alpha, me, rhoDM, rho = unbox_constants(physicsvalues_obj,target)
    
    # ALP rate proportional to gae^2
    gaeSq0 = np.power(gae0,2.0)
    
    
    # compute the ALP rate
    # ALP prescale units: [GeV/cm3]*[keV/GeV]*[eV]*[keV]*[m^3/kg]*[cm^3/m^3]*[kg/g]*[1/eVs][1/keV^2]*[s/day] = per g-day.
    ALP_rate = 3.0*rhoDM*1e6*gaeSq0*sig1*abs_masses*(60*60*24)/(rho*1e-3*hbar*16.0*np.pi*alpha*(me**2.0))
    if not include_inmedium_effects:
        ALP_rate *= 1/n_use
    return ALP_rate