# Absorption Models Repository - Release Notes
Contact - Taylor Aralis (taralis@caltech.edu, tbaralis@gmail.com), Matthew Wilson (matthew.wilson@kit.edu)

Record of releases and tags for the Absorption Models repository. The tagging procedure follows the [Semantic Versioning](https://semver.org/) method.

---

## v2.1.5
**Date:** September 2, 2023

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Fixed bug in setup file in the get_package_version_hist function. In some cases, git url could not be resolved.

**Changes from Last Release Version:** No change to the functionality from previous version.

---

## v2.1.4
**Date:** September 1, 2023

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Updated README and installation instructions based on the new name of the public version of this package, as well as public urls to dependent packages. Updated names and urls in setup file.

**Changes from Last Release Version:** No change to the functionality from previous version.

---

## v2.1.3
**Date:** August 17, 2023

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Fixed bug in ALP model - when input sig1 is used and include_inmedium_effects is False, index of refraction data was not loaded. This is fixed now.

**Changes from Last Release Version:** No change to the functionality from previous version - an error no longer occurs in the ALP model when an input sig1 is used and include_inmedium_effects is False.

---

## v2.1.2
**Date:** August 15, 2023

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Updated get_package_version_hist function in setup file to avoid installing tagged versions that do not begin with 'v'. Updated init file so that package version can be read after import.

**Changes from Last Release Version:** No change to the functionality from previous version.

---


## v2.1.1
**Date:** August 15, 2023

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Updates based on ORC review process for this software package.

**Changes from Last Release Version:** Fixed typos in README and added additional information. Added statement in LICENSE. The 'temp' input argument has been renamed to 'temperature'. The input argument 'target' is now an optional argument with the default set to 'Si'. This package now requires version 1.2.5 of opticalconstants.

---


## v2.1.0
**Date:** May 12, 2023

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Changed the default rate calculation for ALP absorption to follow the approach in [Hochberg et. al](https://journals.aps.org/prd/abstract/10.1103/PhysRevD.95.023013) instead of the previous description. The Hochberg et. al. approach accounts for in-medium effects. The only difference between the two approaches is a factor of $n$, the index of refraction of the target material. See the README for more details.

An optional input argument `include_inmedium_effects` was introduced to toggle between the two approaches. If `False`, the ALP rate calculation reverts back to the previous approach (without the dependence on $n$). The input arguments `pexsec` and `pexsec_unc` have been renamed to `sig1` and `sig1_unc`, respectively, to match formalism in [Hochberg et. al](https://journals.aps.org/prd/abstract/10.1103/PhysRevD.95.023013).

Updated path of physicsvales package in setup file. Updated Issue Tracking section of README.

**Changes from Last Release Version:** Default ALP rate calculation has an additional factor of $n$ to follow the approach of [Hochberg et. al](https://journals.aps.org/prd/abstract/10.1103/PhysRevD.95.023013). The previous approach can still be used by setting `include_inmedium_effects` to `False`. The input arguments `pexsec` and `pexsec_unc` have been renamed to `sig1` and `sig1_unc`, respectively.

## v2.0.2
**Date:** Feb 13, 2023

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Found bug in argument checks. Line breaks were added but string conversions "%s" were not placed properly. Bugs are fixed.

**Changes from Last Release Version:** No changed to functionality from last version.


---

## v2.0.1
**Date:** Feb 11, 2023

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Fixed some bugs with latex math rendering in README. Added sentence in README that this package is used by SuperCDMS.

**Changes from Last Release Version:** No changed to functionality from last version.

---

## v2.0.0
**Date:** Feb 10, 2023

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Simplified package to include only tools for computing the dark photon absorption rate or ALP absorption rate for a given dark photon/ALP mass, target material, and mixing parameter/axio-electric coupling constant.

Removed all components related to detector response. Removed all files related to HVeV analyses. 

Incorporated other custom packages that this package is dependent on (physicsvalues and optical constants).

Updated README, added gitignore file. Updated setup file. Added _version.py file.

**Changes from Last Release Version:** Functions are now different. Only generate_DPA_rate and generate_ALP_rate functions exist. No detector response components are involved. No files from HVeV analyses are included. Parameters and constants are now found through other packages.

---


## v1.0.0
**Date:** Nov 17, 2022

**Author and Email:** Taylor Aralis (taralis@caltech.edu, tbaralis@gmail.com)

**Comments:** Packaging, adding actual info to README, adding release notes.

**Changes from Last Release Version:** N/A
